# FreeRead alternative for Google's Good reads 😉

Test et démo pour la gestion du développement d'un proet web avec gitlab, Symfony 6, et plein d'autre outils super efficaces, marrants et sympa à utiliser !

##  5. <a name='Licence'></a>Licence

Ce dépôt est sous licence [MIT](LICENSE)

##  6. <a name='Auteur'></a>Auteur

[Romaric PIBOLLEAU - RP2I](https://rp2i.net)